class ImplementationError extends Error {
  /**
   * ImplementationError constructor
   * @param {string} message
   */
  constructor(message) {
    super(message);
    this.name = 'ImplementationError';
  }
}

class Shape {
  /**
   * Shape constructor
   * @param {string} color
   * @param {boolean} filled
   */
  constructor(color, filled) {
    this.color = color;
    this.filled = filled;
  }

  /**
   * Get color of this shape
   * @returns {string}
   */
  getColor() {
    return this.color;
  }

  /**
   * Set color of this shape
   * @param {string} color
   */
  setColor(color) {
    this.color = color;
  }

  /**
   * Check if this shape is filled
   * @returns {boolean}
   */
  isFilled() {
    return this.filled;
  }

  /**
   * Set filled property of this shape
   * @param {boolean} filled
   */
  setFilled(filled) {
    this.filled = filled;
  }

  /**
   * Abstract method to calculate
   * area of the concrete shape
   * @returns {number}
   */
  getArea() {
    throw new ImplementationError('Bu obyektin sahəsini hesablaya bilmirik.');
  }

  /**
   * Abstract method to calculate
   * perimeter of the concrete shape
   * @returns {number}
   */
  getPerimeter() {
    throw new ImplementationError(
      'Bu obyektin perimetrini hesablaya bilmirik.'
    );
  }

  /**
   * Abstract method to draw
   * the concrete shape on Canvas
   * @param {CanvasRenderingContext2D}
   * @returns {void}
   */
  draw(ctx) {
    throw new ImplementationError('Bu obyektin şəklini çəkə bilmirik.');
  }
}

class Circle extends Shape {
  /**
   * Circle constructor
   * @param {number} radius
   * @param {string} color
   * @param {boolean} filled
   */
  constructor(radius, color, filled) {
    super(color, filled);
    this.radius = radius;
  }

  /**
   * Returns radius of this circle
   * @returns {number}
   */
  getRadius() {
    return this.radius;
  }

  /**
   * Sets radius of the circle
   * @param {number} radius
   */
  setRadius(radius) {
    this.radius = radius;
  }

  /**
   * Calculate area of the circle
   * @returns {number}
   */
  getArea() {
    return Math.PI * this.radius * this.radius;
  }

  /**
   * Calculate the length of the circle
   * @returns {number}
   */
  getPerimeter() {
    return 2 * Math.PI * this.radius;
  }

  /**
   * Draws the circle on canvas's center
   * @param {CanvasRenderingContext2D} ctx
   */
  draw(ctx) {
    ctx.clearRect(0, 0, 500, 500);

    ctx.arc(250, 250, this.radius, 0, 2 * Math.PI);
    if (this.filled) {
      ctx.fillStyle = this.color;
      ctx.fill();
    } else {
      ctx.strokeStyle = this.color;
      ctx.stroke();
    }
  }
}

class Rectangle extends Shape {
  /**
   * Rectangle constructor
   * @param {number} width
   * @param {number} height
   * @param {string} color
   * @param {boolean} filled
   */
  constructor(width, height, color, filled) {
    super(color, filled);
    this.width = width;
    this.height = height;
  }

  /**
   * Get width of this rectangle
   * @returns {number}
   */
  getWidth() {
    return this.width;
  }

  /**
   * Set width of this rectangle
   * @param {number} width
   */
  setWidth(width) {
    this.width = width;
  }

  /**
   * Get height of this rectangle
   * @returns {number}
   */
  getHeight() {
    return this.height;
  }

  /**
   * Set height of this rectangle
   * @param {number} height
   */
  setHeight(height) {
    this.height = height;
  }

  /**
   * Get area of this rectangle
   * @returns {number}
   */
  getArea() {
    return this.width * this.height;
  }

  /**
   * Get perimeter of this rectangle
   * @returns {number}
   */
  getPerimeter() {
    return 2 * (this.width + this.height);
  }

  /**
   * Draws the rectangle on canvas's center
   * @param {CanvasRenderingContext2D} ctx
   */
  draw(ctx) {
    ctx.clearRect(0, 0, 500, 500);

    ctx.rect(250, 250, this.width, this.height);
    if (this.filled) {
      ctx.fillStyle = this.color;
      ctx.fill();
    } else {
      ctx.strokeStyle = this.color;
      ctx.stroke();
    }
  }
}

class Square extends Rectangle {
  /**
   * Square constructor
   * @param {number} side
   * @param {string} color
   * @param {boolean} filled
   */
  constructor(side, color, filled) {
    super(side, side, color, filled);
  }

  /**
   * Get side of this square
   * @returns {number}
   */
  getSide() {
    return this.width;
  }

  /**
   * Sets side of this square
   * @param {number} side
   */
  setSide(side) {
    this.height = side;
    this.width = side;
  }

  /**
   * Sets width of this square
   * @param {number} side
   */
  setWidth(side) {
    this.setSide(side);
  }

  /**
   * Sets height of this square
   * @param {number} side
   */
  setHeight(side) {
    this.setSide(side);
  }
}

class Cylinder extends Shape {
  constructor(radius, height) {
    this.base = new Circle(radius);
    this.height = height;
  }

  getArea() {
    return (
      2 * Math.PI * this.base.getRadius() * this.height +
      2 * this.base.getArea()
    );
  }
}

// -- CANVAS CODE --
const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

const circle = new Circle(50, 'green', true);
const rectangle = new Rectangle(50, 60, 'red', false);
const sqaure = new Square(100, 'yellow', true);
